import cv2
import numpy as np
from matplotlib import pyplot as plt


def translation(input_img,height,width):
    # usar na matriz, o quanto translacao
    trans_height = height/4
    trans_width = width/4

    # necessario para matriz de translacao
    #     | 1 0 Tx |   
    # T = | 0 1 Ty |

    T = np.float32([[1,0, trans_width],[0,1,trans_height]])
    
    trans_input_img = cv2.warpAffine(input_img,T,(width,height))
    cv2.imwrite('output/translation.jpg', trans_input_img)

def rotation(input_img,height,width):
    # matriz de rotacao, onde 0 eh o angulo
    #     | cos0 -sin0|
    # R = | sin0  cos0|
    # R parameter: rotation_x, rotation_y,angle,scale)    
    
    R = cv2.getRotationMatrix2D((width/2,height/2),90,1)

    rot_input_img = cv2.warpAffine(input_img,R,(width,height))
    cv2.imwrite('output/rotation.jpg', rot_input_img)

def interpolation(input_img,height,width):
    # interpolation: eh um metodo para construir novos data points
    # dentro de um alcance de um conjunto discretos de data points conhecidos
    
    # diminui a dimensao em 25 porcento    
    input_img = cv2.resize(input_img, None, fx=0.75, fy=0.75)
    cv2.imwrite('output/interpolation_linear.jpg', input_img)

    # aumenta a dimensao 2 vezes
    input_img = cv2.resize(input_img, None, fx=2, fy=2, interpolation = cv2.INTER_CUBIC)
    cv2.imwrite('output/interpolation_cubic.jpg', input_img)

    # coloca a dimensao exata
    input_img = cv2.resize(input_img, (900,400), interpolation = cv2.INTER_AREA)
    cv2.imwrite('output/interpolation_area.jpg', input_img)


def pyramid(input_img,height,width):
    # divide por 2
    smaller = cv2.pyrDown(input_img)
    
    # multiplica por 2
    larger = cv2.pyrUp(smaller)

    cv2.imwrite("output/smaller.jpg",smaller)
    cv2.imwrite("output/larger.jpg",larger)

def copping(input_img,height,width):
    # posicao que inicia para o corte    
    s_row = int(height * .20)
    s_col = int(width*.35)
    
    # posicao que finaliza o corte
    f_row = int(height * .50)
    f_col = int(width*.65)

    crop = input_img[s_row: f_row, s_col: f_col]
    cv2.imwrite("output/crop.jpg",crop)

def operation(input_img,height,width):
    # cria matriz mesma dimensao
    ones = np.ones(input_img.shape, dtype="uint8") * 80
    
    # aumenta o brilho
    add = cv2.add(input_img,ones)
    cv2.imwrite("output/add.jpg",add)

    # diminui o brilho
    sub = cv2.subtract(input_img,ones)
    cv2.imwrite("output/sub.jpg",sub)

    # mascara quadrada
    square = np.zeros((300,300), np.uint8)
    cv2.rectangle(square,(50,50),(250,250),255,-2)
    cv2.imwrite("output/square.jpg",square)

    # mascara ellipse
    ellipse = np.zeros((300,300), np.uint8)
    cv2.ellipse(ellipse,(150,150),(150,150),0,0,180,255,-1)
    cv2.imwrite("output/ellipse.jpg",ellipse)

    # onde interseccao
    and_op = cv2.bitwise_and(square,ellipse)
    cv2.imwrite("output/and_op.jpg",and_op)

    # onde temos elipse ou quadrado
    or_op = cv2.bitwise_or(square,ellipse)
    cv2.imwrite("output/or_op.jpg",or_op)

    # onde temos um deles xor
    xor_op = cv2.bitwise_xor(square,ellipse)
    cv2.imwrite("output/xor_op.jpg",xor_op)

    # onde temos tudo que nao esta no quadrado
    not_op = cv2.bitwise_not(square)
    cv2.imwrite("output/not_op.jpg",not_op)

def convolution(input_img):
    # convolucao: eh uma operacao matematica realizada sobre duas funcoes que produz uma terceira
    
    # criamos o kernel e dividimos por 9 para normalizar.    
    kernel = np.ones((3,3),np.float32)/9

    # aplicamos convolucao
    blurred = cv2.filter2D(input_img,-1,kernel)
    cv2.imwrite("output/blur3x3.jpg",blurred)

    # criamos o kernel e dividimos por 49 para normalizar.    
    kernel = np.ones((7,7),np.float32)/49

    # aplicamos convolucao
    blurred = cv2.filter2D(input_img,-1,kernel)
    cv2.imwrite("output/blur7x7.jpg",blurred)

    # outras formas de usar blur
    bilateral = cv2.bilateralFilter(input_img,10,75,75)
    cv2.imwrite("output/blurbilateral.jpg",bilateral)

    # usa a media de todos elementos de uma janela
    median = cv2.medianBlur(input_img,5)
    cv2.imwrite("output/blurmedian.jpg",median)

    # similar a cv2.blur mas uma janela gaussiana
    gaussian = cv2.GaussianBlur(input_img,(7,7),0)
    cv2.imwrite("output/blurgaussian.jpg",gaussian)

    # media dos valores de uma especifica janela
    blurred = cv2.blur(input_img,(15,15))
    cv2.imwrite("output/blur15x15.jpg",blurred)

def sharpening(input_img):
    #           | -1  -1  -1 |
    # Kernel =  | -1   9  -1 |
    #           | -1  -1  -1 |
    # Da enfase nas bordas. 
    # A soma do kernel tem de ser 1
    S = np.array([[-1,-1,-1],
                 [-1,9,-1],
                 [-1,-1,-1]])
    
    sharp_filter = cv2.filter2D(input_img,-1,S)
    cv2.imwrite("output/sharp.jpg",sharp_filter)

def thresholding():
    # imagem em cinza
    input_img = cv2.imread("./images/photo.jpg",0)

    # Thresholding eh o ato de converte a imagem para a forma binaria
    # valores abaixo de 127 vao para 0, e acima para 255    
    ret, thresh1 = cv2.threshold(input_img,127,255,cv2.THRESH_BINARY)
    cv2.imwrite("output/thresh1.jpg",thresh1)

    # valores abaixo de 127 vao para 255, e acima para 0
    ret, thresh2 = cv2.threshold(input_img,127,255,cv2.THRESH_BINARY_INV)
    cv2.imwrite("output/thresh2.jpg",thresh2)

    # valores acima de 127 sao mantido em 127
    ret, thresh3 = cv2.threshold(input_img,127,255,cv2.THRESH_TRUNC)
    cv2.imwrite("output/thresh3.jpg",thresh3)

    # abaixo de 127 muda para zero
    ret, thresh4 = cv2.threshold(input_img,127,255,cv2.THRESH_TOZERO)
    cv2.imwrite("output/thresh4.jpg",thresh4)

    # acima de 127 muda para zero
    ret, thresh5 = cv2.threshold(input_img,127,255,cv2.THRESH_TOZERO_INV)
    cv2.imwrite("output/thresh5.jpg",thresh5)

def morphology():
    # Dilation, erosion, opening and closing
    
    input_img = cv2.imread("./images/trek.jpg",0)
    kernel = np.ones((5,5),np.uint8)
   
    erosion = cv2.erode(input_img,kernel,iterations = 1)
    cv2.imwrite("output/erosion.jpg",erosion)

    dilation = cv2.dilate(input_img,kernel,iterations = 1)
    cv2.imwrite("output/dilation.jpg",dilation)

    # remove ruido
    opening = cv2.morphologyEx(input_img,cv2.MORPH_OPEN,kernel)
    cv2.imwrite("output/opening.jpg",opening)

    # remove ruido
    closing = cv2.morphologyEx(input_img,cv2.MORPH_CLOSE,kernel)
    cv2.imwrite("output/closing.jpg",closing)

def edge_detected():
    # examplos: sobel, laplacian, canny.
    # algoritmo:
    # 1) aplica gaussiano blur
    # 2) encontra a intensidade do gradiente
    # 3) remove pixels que nao sao da borda
    # 4) pixels que sao upper ou lower sao considerados aresta

    # imagem em cinza
    input_img = cv2.imread("./images/photo.jpg",0)
    
    #sobel
    height, width = input_img.shape
    sobel_x = cv2.Sobel(input_img, cv2.CV_64F,0,1, ksize=5)
    cv2.imwrite("output/sobel_x.jpg",sobel_x)
    sobel_y = cv2.Sobel(input_img, cv2.CV_64F,1,0, ksize=5)
    cv2.imwrite("output/sobel_y.jpg",sobel_y)

    sobel_OR = cv2.bitwise_or(sobel_x,sobel_y)
    cv2.imwrite("output/sobel_OR.jpg",sobel_OR)

    laplacia = cv2.Laplacian(input_img, cv2.CV_64F)
    cv2.imwrite("output/laplacia.jpg",laplacia)

    canny = cv2.Canny(input_img, 20,170)
    cv2.imwrite("output/canny.jpg",canny)

def afine(input_img,height,width):
    # cordenada da imagem principal
    points = np.float32([[320,15],[700,215],[85,610],[530,780]])
    # cordenada da saida    
    points1 = np.float32([[0,0],[420,0],[0,594],[420,594]])

    M = cv2.getPerspectiveTransform(points, points1)
    perspective = cv2.warpPerspective(input_img,M,(420,594))
    cv2.imwrite("output/perspective.jpg",perspective)   

    # cordenada da imagem principal
    points = np.float32([[320,15],[700,215],[85,610]])
    # cordenada da saida    
    points1 = np.float32([[0,0],[420,0],[0,594]])
    M = cv2.getAffineTransform(points,points1)
    affine = cv2.warpAffine(input_img,M,(height,width))
    cv2.imwrite("output/affine.jpg",affine) 

def main():
    # Le imagem    
    input_img = cv2.imread("./images/photo.jpg")

    # Affine: scaling, rotation, translation
    # Non-affine: nao preserva paralelismo, tamanho e angulo

    # ignora dimensao das cores
    height, width = input_img.shape[:2]
    
    translation(input_img,height,width)
    rotation(input_img,height,width)
    interpolation(input_img,height,width)   
    pyramid(input_img,height,width)
    copping(input_img,height,width)
    operation(input_img,height,width)
    convolution(input_img) 
    sharpening(input_img) 
    thresholding()
    morphology()
    edge_detected()
    afine(input_img,height,width)

# Inicia
if __name__ == "__main__":
    main()
