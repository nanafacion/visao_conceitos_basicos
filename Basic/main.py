import cv2
import numpy as np
from matplotlib import pyplot as plt


def image_hsv(input):
    # Em hsv
    hsv = cv2.cvtColor(input, cv2.COLOR_BGR2HSV)  
    cv2.imshow("output/HSV_image.jpg", hsv)
    cv2.imwrite("output/Hue_channel.jpg", hsv[:,:,0])
    cv2.imwrite("output/Saturation_channel.jpg", hsv[:,:,1])
    cv2.imwrite("output/Value_channel.jpg", hsv[:,:,2])

def image_gray(input):
    # Em tom de cinza
    gray = cv2.cvtColor(input, cv2.COLOR_BGR2GRAY)    
    # Salva imagem em cinza
    cv2.imwrite("output/gray.jpg",gray)

def image_channel(input):
    # Separa os canais R , G , B
    B,G,R = cv2.split(input)
    # Cria zeros para mergear channels
    zeros = np.zeros(input.shape[:2],dtype= "uint8")
    cv2.imwrite("output/Blue.jpg", cv2.merge([B,zeros,zeros]))
    cv2.imwrite("output/Green.jpg", cv2.merge([zeros,G,zeros]))
    cv2.imwrite("output/Red.jpg", cv2.merge([zeros,zeros,R]))
    # Junta os channels
    cv2.imwrite("output/BGR.jpg", cv2.merge([B,G,R]))

def histogram(input):
    histogram = cv2.calcHist([input],[0], None, [256],[0,256])
    plt.hist(input.ravel(),256,[0,256]);
    #plt.show()
    plt.savefig("output/histgray.jpg")
    plt.close()

    for i,col in enumerate(('b','g','r')):
        hist = cv2.calcHist([input],[i],None,[256],[0,256])
        plt.plot(hist,color=col)
        plt.xlim([0,256])

    #plt.show()
    plt.savefig("output/colors.jpg")
    plt.close()

def draw():
    image = np.zeros((512,512,3),np.uint8)
    # desenha linha na posicao inicial e final, com as cores RGB de tamanho 5 pixel.
    cv2.line(image,(0,0),(511,511),(255,250,0),5)
    # rectangle( image, inicio, fim, cor, thickness)
    cv2.rectangle(image,(100,100),(300,500),(127,50,127),5-1)
    # circle( image, center, radius, color, fill)
    cv2.circle(image,(350,350),100,(15,75,50),-1)
    # polygons (points)
    pts = np.array([[10,50],[400,50],[90,150],[100,210]],np.int32)
    pts = pts.reshape((-1,1,2))
    cv2.polylines(image,[pts],True,(250,0,255),3)
    # text
    cv2.putText(image,'Star Trek',(50,50), cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),1,cv2.LINE_AA)
    cv2.imwrite("output/draw.jpg",image)

def main():
    # Le imagem    
    input = cv2.imread("./images/photo.jpg")

    # Abre em janela
    #cv2.imshow("Star trek", input)
    #cv2.waitKey(0)

    # Dimensoes da imagem
    print("Altura:", input.shape[0])
    print("Largura:", input.shape[1])
    print("Cores:",input.shape[2])

    # chama metodos
    image_hsv(input)
    image_gray(input)
    image_channel(input)
    histogram(input)
    draw()


# Inicia
if __name__ == "__main__":
    main()
