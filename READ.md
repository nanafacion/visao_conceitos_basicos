## Objetivo

Esse projeto é usado para o aprendizado de uso de opencv2, para as áreas de visão computacional e aprendizado de máquina.

## Instalação

É necessário o uso da Anaconda. Acesse o site https://docs.anaconda.com/anaconda/install/linux para mais informações de como instalar.
Entre em alguma das pastas e digite : python main.py
